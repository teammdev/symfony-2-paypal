<?php

namespace Mdev\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mdev\ShopBundle\Entity\Order;
use JMS\Payment\CoreBundle\Entity\ExtendedData;
use JMS\Payment\CoreBundle\Form\ChoosePaymentMethodType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use JMS\Payment\CoreBundle\Plugin\Exception\Action\VisitUrl;
use JMS\Payment\CoreBundle\Plugin\Exception\ActionRequiredException;
use JMS\Payment\CoreBundle\PluginController\Result;
use Mdev\ShopBundle\Form\CreditCardType;

class OrdersController extends Controller
{
	

	

    public function indexAction()
    {
        return $this->render('MdevShopBundle:Default:index.html.twig');
    }

    public function newAction($amount)
	{
	    $em = $this->getDoctrine()->getManager();

	    $order = new Order($amount);
	    $em->persist($order);
	    $em->flush();

	    return $this->redirect($this->generateUrl('mdev_shop_show', [
	        'id' => $order->getId(),
	    ]));
	}

	public function showAction(Request $request, Order $order)
	{
		$config = [
		    'stripe_checkout' => [
		        'description' => 'My test paypal payment',
		    ],
		    'paypal_express_checkout' => [
		        'return_url' => $this->generateUrl('mdev_shop_payment_complete', [
		            'id' => $order->getId(),
		        ], UrlGeneratorInterface::ABSOLUTE_URL),
		        'cancel_url' => $this->generateUrl('mdev_shop_payment_canceled', [
		            'id' => $order->getId(),
		        ], UrlGeneratorInterface::ABSOLUTE_URL),
		        'useraction' => 'commit',
		    ],
		];

	    $form = $this->createForm(ChoosePaymentMethodType::class, null, [
	        'amount'   => $order->getAmount(),
	        'currency' => 'EUR',
	        'predefined_data' => $config,
	        'allowed_methods' => ['paypal_express_checkout', 'credit_card'],
	        'default_method'  => 'credit_card',
	    ]);

	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
	        $ppc = $this->get('payment.plugin_controller');
	        $ppc->createPaymentInstruction($instruction = $form->getData());

	        $order->setPaymentInstruction($instruction);

	        $em = $this->getDoctrine()->getManager();
	        $em->persist($order);
	        $em->flush($order);

	        return $this->redirect($this->generateUrl('mdev_shop_payment_create', [
	            'id' => $order->getId(),
	        ]));
	    }

	    return $this->render('MdevShopBundle:Orders:show.html.twig', array(
	        'order' => $order,
	        'form' => $form->createView(),
	    ));
	}

	private function createPayment($order)
	{
	    $instruction = $order->getPaymentInstruction();
	    $pendingTransaction = $instruction->getPendingTransaction();

	    if ($pendingTransaction !== null) {
	        return $pendingTransaction->getPayment();
	    }

	    $ppc = $this->get('payment.plugin_controller');
	    $amount = $instruction->getAmount() - $instruction->getDepositedAmount();

	    return $ppc->createPayment($instruction->getId(), $amount);
	}

	public function paymentCreateAction(Order $order)
	{
	    $payment = $this->createPayment($order);

	    $ppc = $this->get('payment.plugin_controller');
	    $result = $ppc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());

	    
	    if ($result->getStatus() === Result::STATUS_PENDING) {
		    $ex = $result->getPluginException();

		    if ($ex instanceof ActionRequiredException) {
		        $action = $ex->getAction();

		        if ($action instanceof VisitUrl) {
		            return $this->redirect($action->getUrl());
		        }

		        throw $ex;
		    }
		}
		
	    if ($result->getStatus() === Result::STATUS_SUCCESS) {
	        return $this->redirect($this->generateUrl('mdev_shop_payment_complete', [
	            'id' => $order->getId(),
	        ]));
	    }

	    throw new \Exception('Transaction was not successful: '.$result->getReasonCode());
	}

	public function paymentCompleteAction(Order $order)
	{
	    return new Response('Payment complete! Bravo! ');
	}

	public function paymentCanceledAction(Order $order)
	{
	    return new Response('Payment canceled! ');
	}


}
