<?php

namespace Mdev\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MdevShopBundle:Default:index.html.twig');
    }
}
